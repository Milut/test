package ru.mail.milut.test.leetcode;

/**
 * 26. Remove Duplicates from Sorted Array
 * <p>
 * Given an integer array nums sorted in non-decreasing order, remove the duplicates in-place
 * such that each unique element appears only once. The relative order of the elements should
 * be kept the same. Then return the number of unique elements in nums.
 * <p>
 * Consider the number of unique elements of nums to be k, to get accepted, you need to do the
 * following things:
 * <ul>
 *     <li>Change the array nums such that the first k elements of nums contain the unique
 *     elements in the order they were present in nums initially. The remaining elements of
 *     nums are not important as well as the size of nums.
 *     <li>Return k.
 * </ul>
 * <a href="https://leetcode.com/problems/remove-duplicates-from-sorted-array/">https://leetcode.com/problems/remove-duplicates-from-sorted-array/</a>
 * <br/>
 * <a href="https://leetcode.com/submissions/detail/841670564/">https://leetcode.com/submissions/detail/841670564/</a>
 * <br/>
 * <a href="https://leetcode.com/problems/remove-duplicates-from-sorted-array/discuss/2805101/Java-One-pointer-in-place-solution">https://leetcode.com/problems/remove-duplicates-from-sorted-array/discuss/2805101/Java-One-pointer-in-place-solution</a>
 */
public class RemoveDuplicatesFromSortedArray {
    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0 || nums.length == 1) {
            return nums.length;
        }
        int k = 1;
        for (int i = 0; i < nums.length - 1; i++) {
            int currentValue = nums[i];
            int nextValue = nums[i + 1];
            if (currentValue != nextValue) {
                nums[k] = nextValue;
                k++;
            }
        }
        return k;
    }

    public static void testRemoveDuplicates() {
        report(new int[]{}, new int[]{});
        report(new int[]{1}, new int[]{1});
        report(new int[]{1, 1}, new int[]{1});
        report(new int[]{1, 1, 1}, new int[]{1});
        report(new int[]{1, 1, 1, 1}, new int[]{1});
        report(new int[]{1, 2}, new int[]{1, 2});
        report(new int[]{1, 2, 3}, new int[]{1, 2, 3});
        report(new int[]{1, 2, 3, 4}, new int[]{1, 2, 3, 4});
        report(new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 5});
        report(new int[]{1, 2, 2}, new int[]{1, 2});
        report(new int[]{1, 2, 2, 3, 4, 4, 4, 4}, new int[]{1, 2, 3, 4});
        report(new int[]{1, 1, 2}, new int[]{1, 2});
        report(new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4}, new int[]{0, 1, 2, 3, 4});
    }

    private static void report(int[] nums, int[] expectedNums) {
        int k = removeDuplicates(nums);
        assert k == expectedNums.length;
        for (int i = 0; i < k; i++) {
            assert nums[i] == expectedNums[i];
        }
    }

    public static void main(String[] args) {
        testRemoveDuplicates();
    }
}
