package ru.mail.milut.test.leetcode;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 1544. Make The String Great
 * <p>
 * Given a string s of lower and upper case English letters.
 * <p>
 * A good string is a string which doesn't have two adjacent characters s[i] and s[i + 1] where:
 * <ul>
 *     <li>0 <= i <= s.length - 2
 *     <li>s[i] is a lower-case letter and s[i + 1] is the same letter but in upper-case or vice-versa.
 * </ul>
 * To make the string good, you can choose two adjacent characters that make the string bad and
 * remove them. You can keep doing this until the string becomes good.
 * <p>
 * Return the string after making it good. The answer is guaranteed to be unique under the given
 * constraints.
 * <p>
 * Notice that an empty string is also good.
 * <br/>
 * <a href="https://leetcode.com/problems/make-the-string-great/">https://leetcode.com/problems/make-the-string-great/</a>
 * <br/>
 * <a href="https://leetcode.com/submissions/detail/839750470/">https://leetcode.com/submissions/detail/839750470/</a>
 * <br/>
 * <a href="https://leetcode.com/problems/make-the-string-great/discuss/2804936/Java-Honest-solution!-recursive">https://leetcode.com/problems/make-the-string-great/discuss/2804936/Java-Honest-solution!-recursive</a>
 */
public class MakeTheStringGreat {
    @FunctionalInterface
    private interface MakeGood {
        String makeGood(String badString);
    }

    public static final String EMPTY = "";

    public static String makeGoodStringRecursive(String badString) {
        if (badString.isEmpty() || badString.length() == 1) {
            return badString;
        }
        for (int currentIndex = 0; currentIndex <= badString.length() - 2; currentIndex++) {
            int nextIndex = currentIndex + 1;
            int nextNextIndex = currentIndex + 2;
            char currentChar = badString.charAt(currentIndex);
            char nextChar = badString.charAt(nextIndex);
            String currentLetter = String.valueOf(currentChar);
            String nextLetter = String.valueOf(nextChar);
            if (currentLetter.equalsIgnoreCase(nextLetter)
                    && (Character.isUpperCase(currentChar) && Character.isLowerCase(nextChar)
                    || Character.isLowerCase(currentChar) && Character.isUpperCase(nextChar))) {
                if (badString.length() == 2) {
                    return EMPTY;
                } else {
                    return makeGoodStringRecursive(
                            (currentIndex == 0 ? EMPTY : badString.substring(0, currentIndex))
                                    + badString.substring(nextNextIndex));
                }
            }
        }
        return badString;
    }

    public static String makeGoodListRecursive(String badString) {
        if (badString.isEmpty() || badString.length() == 1) {
            return badString;
        }
        for (int currentIndex = 0; currentIndex <= badString.length() - 2; currentIndex++) {
            int nextIndex = currentIndex + 1;
            char currentChar = badString.charAt(currentIndex);
            char nextChar = badString.charAt(nextIndex);
            String currentLetter = String.valueOf(currentChar);
            String nextLetter = String.valueOf(nextChar);
            if (currentLetter.equalsIgnoreCase(nextLetter)
                    && (Character.isUpperCase(currentChar) && Character.isLowerCase(nextChar)
                    || Character.isLowerCase(currentChar) && Character.isUpperCase(nextChar))) {
                if (badString.length() == 2) {
                    return EMPTY;
                } else {
                    List<Character> badStringChars = badString.chars()
                            .mapToObj(e -> (char) e).collect(Collectors.toList());
                    badStringChars.remove(Character.valueOf(currentChar));
                    badStringChars.remove(Character.valueOf(nextChar));
                    return makeGoodListRecursive(
                            badStringChars.stream().map(String::valueOf).collect(Collectors.joining()));
                }
            }
        }
        return badString;
    }

    public static String makeGoodIterative(String badString) {
        if (badString.isEmpty() || badString.length() == 1) {
            return badString;
        }
        StringBuilder stringBuilder = new StringBuilder(badString);
        int currentIndex = 0;
        while (currentIndex <= stringBuilder.length() - 2) {
            int nextIndex = currentIndex + 1;
            char currentChar = stringBuilder.charAt(currentIndex);
            char nextChar = stringBuilder.charAt(nextIndex);
            String currentLetter = String.valueOf(currentChar);
            String nextLetter = String.valueOf(nextChar);
            if (currentLetter.equalsIgnoreCase(nextLetter)
                    && (Character.isUpperCase(currentChar) && Character.isLowerCase(nextChar)
                    || Character.isLowerCase(currentChar) && Character.isUpperCase(nextChar))) {
                if (stringBuilder.length() == 2) {
                    return EMPTY;
                } else {
                    stringBuilder.delete(currentIndex, nextIndex + 1);
                    currentIndex = 0;
                }
            } else {
                currentIndex++;
            }
        }
        return stringBuilder.toString();
    }

    public static void testMakeGood(MakeGood makeGood) {
        report(EMPTY, EMPTY, makeGood);
        report("s", "s", makeGood);
        report("ss", "ss", makeGood);
        report("sss", "sss", makeGood);
        report("ssS", "s", makeGood);
        report("ssss", "ssss", makeGood);
        report("sssS", "ss", makeGood);
        report("sssSS", "s", makeGood);
        report("sssSsS", "ss", makeGood);
        report("sS", EMPTY, makeGood);
        report("Ss", EMPTY, makeGood);
        report("SS", "SS", makeGood);
        report("sSs", "s", makeGood);
        report("ssSs", "ss", makeGood);
        report("sSss", "ss", makeGood);
        report("Ssss", "ss", makeGood);
        report("SSS", "SSS", makeGood);
        report("leEeetcode", "leetcode", makeGood);
        report("abBAcC", EMPTY, makeGood);
    }

    private static void report(String badString, String goodString, MakeGood makeGood) {
        String goodStringMaid = makeGood.makeGood(badString);
        assert goodStringMaid.equals(goodString)
                : String.format("[%s] -> [%s] instead of [%s]", badString, goodStringMaid, goodString);
        System.out.printf("[%s] -> [%s]\n", badString, goodStringMaid);
    }

    public static void main(String[] args) {
        System.out.println("makeGoodStringRecursive()");
        testMakeGood(MakeTheStringGreat::makeGoodStringRecursive);
        System.out.println("makeGoodListRecursive()");
        testMakeGood(MakeTheStringGreat::makeGoodListRecursive);
        System.out.println("makeGoodIterative()");
        testMakeGood(MakeTheStringGreat::makeGoodIterative);
    }
}
