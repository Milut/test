package ru.mail.milut.test.reverse;

import ru.mail.milut.test.node.list.Node;

import static ru.mail.milut.test.Utils.initList;
import static ru.mail.milut.test.Utils.printList;

public class ReverseList {
    @FunctionalInterface
    private interface Reverser {
        Node reverse(Node list);
    }

    private static Node reverseSingleLinkedList(Node list) {
        if (!list.hasNext()) {
            return list;
        }
        Node currentNode = list.getNext();
        Node previousNode = list;
        previousNode.setNext(null);
        while (currentNode.hasNext()) {
            Node nextNode = currentNode.getNext();
            currentNode.setNext(previousNode);
            previousNode = currentNode;
            currentNode = nextNode;
        }
        currentNode.setNext(previousNode);
        return currentNode;
    }

    private static Node reverseDoubleLinkedList(Node list) {
        if (!list.hasNext()) {
            return list;
        }
        Node currentNode = list.getNext();
        list.setNext(null);
        while (currentNode.hasNext()) {
            Node nextNode = currentNode.getNext();
            currentNode.setNext(currentNode.getPrevious());
            currentNode.setPrevious(nextNode);
            currentNode = nextNode;
        }
        currentNode.setNext(currentNode.getPrevious());
        return currentNode;
    }

    private static void testReverse(Node list, Reverser reverser) {
        printList(list);
        System.out.print(" => ");
        printList(reverser.reverse(list));
        System.out.println();
    }

    private static void testReverse(Reverser reverser) {
        for (int i = 1; i <= 7; i++) {
            testReverse(initList(i), reverser);
        }
    }

    public static void main(String[] args) {
        System.out.println("reverseSingleLinkedList()");
        testReverse(ReverseList::reverseSingleLinkedList);
        System.out.println("reverseDoubleLinkedList()");
        testReverse(ReverseList::reverseDoubleLinkedList);
    }
}
