package ru.mail.milut.test.reverse;

import java.util.Arrays;
import java.util.Collections;

import static ru.mail.milut.test.Utils.swap;

/**
 * Ограничение по памяти O(1)
 * <br>
 * m, u, s, i, c => c, i, s, u, m
 */
public class ReverseArray {
    @FunctionalInterface
    private interface Reverser {
        char[] reverse(char[] array);
    }

    static char[] reverseO1(char[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            swap(array, i, array.length - 1 - i);
        }
        return array;
    }

    static char[] reverseIterative(char[] array) {
        char[] newArray = new char[array.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[array.length - 1 - i];
        }
        return newArray;
    }

    private static void testReverse(char[] array, Reverser reverser) {
        System.out.println(Arrays.toString(array) + " => "
                + Arrays.toString(reverser.reverse(array)));
    }

    private static void testReverse(Reverser reverser) {
        testReverse(new char[]{'m', 'u', 's', 'i', 'c'}, reverser);
        testReverse(new char[]{}, reverser);
        testReverse(new char[]{'1'}, reverser);
        testReverse(new char[]{'1', '2'}, reverser);
        testReverse(new char[]{'1', '2', '3'}, reverser);
        testReverse(new char[]{'1', '2', '3', '4'}, reverser);
        testReverse(new char[]{'1', '2', '3', '4', '5'}, reverser);
        testReverse(new char[]{'1', '2', '3', '4', '5', '6'}, reverser);
        testReverse(new char[]{'1', '2', '3', '4', '5', '6', '7'}, reverser);
    }

    public static void main(String[] args) {
        System.out.println("reverseO1()");
        testReverse(ReverseArray::reverseO1);
        System.out.println("reverseIterative()");
        testReverse(ReverseArray::reverseIterative);
    }
}
