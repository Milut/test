package ru.mail.milut.test.reverse;

/**
 * Ограничение по памяти O(1)
 * <br>
 * music => cisum
 */
public class ReverseString {
    @FunctionalInterface
    private interface Reverser {
        String reverse(String srcString);
    }

    private static String reverseO1(String srcString) {
        return String.valueOf(ReverseArray.reverseO1(srcString.toCharArray()));
    }

    private static String reverseIterativeArray(String srcString) {
        return String.valueOf(ReverseArray.reverseIterative(srcString.toCharArray()));
    }

    private static String reverseIterativeString(String srcString) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = srcString.length() - 1; i >= 0; i--) {
            stringBuilder.append(srcString.charAt(i));
        }
        return stringBuilder.toString();
    }

    private static String reverseStringBuilder(String srcString) {
        return new StringBuilder(srcString).reverse().toString();
    }

    private static void testReverse(String srcString, Reverser reverser) {
        System.out.println(srcString + " => " + reverser.reverse(srcString));
    }

    private static void testReverse(Reverser reverser) {
        testReverse("music", reverser);
        testReverse("", reverser);
        testReverse("1", reverser);
        testReverse("12", reverser);
        testReverse("123", reverser);
        testReverse("1234", reverser);
        testReverse("12345", reverser);
        testReverse("123456", reverser);
        testReverse("1234567", reverser);
    }

    public static void main(String[] args) {
        System.out.println("reverseO1()");
        testReverse(ReverseString::reverseO1);
        System.out.println("reverseIterativeArray()");
        testReverse(ReverseString::reverseIterativeArray);
        System.out.println("reverseIterativeString()");
        testReverse(ReverseString::reverseIterativeString);
        System.out.println("reverseStringBuilder()");
        testReverse(ReverseString::reverseStringBuilder);
    }
}
