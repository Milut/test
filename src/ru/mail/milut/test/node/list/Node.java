package ru.mail.milut.test.node.list;

import java.util.StringJoiner;

public class Node {
    private final Integer value;
    private Node next;
    private Node previous;

    public Node(Integer value, Node next) {
        this.value = value;
        this.next = next;
    }

    public Node(Integer value, Node next, Node previous) {
        this.value = value;
        this.next = next;
        this.previous = previous;
    }

    public boolean hasNext() {
        return next != null;
    }

    public Integer getValue() {
        return value;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getPrevious() {
        return previous;
    }

    public void setPrevious(Node previous) {
        this.previous = previous;
    }

    public void addNode(Integer value) {
        if (!hasNext()) {
            next = new Node(value, null, this);
            return;
        }
        Node nextNode = next;
        while (nextNode.hasNext()) {
            nextNode = nextNode.getNext();
        }
        nextNode.next = new Node(value, null, nextNode);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Node.class.getSimpleName() + "[", "]")
                .add("value=" + value)
                .toString();
    }
}
