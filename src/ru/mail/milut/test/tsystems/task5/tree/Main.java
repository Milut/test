package ru.mail.milut.test.tsystems.task5.tree;

import ru.mail.milut.test.node.tree.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * Задача 5:
 * Необходимо написать метод, получающий на вход бинарное, несбалансированное дерево, которое описывается классом:
 * <p>
 * Метод должен возвращать номер уровня вложенности элемента дерева который содержит элемент с минимальным значением Value во всём дереве.
 * <p>
 * Пример Дерева(сорри за качество):
 * Минимальное число (-4), его уровень вложенности 4.
 * Предполагается, что все числа в дереве разные.
 * <p>
 * 4
 * 12 | -1
 * 0 | 2 7
 * 10 -4 | | 5
 */
public class Main {
    private record MinResult(int minValue, int minValueLevel) {
    }

    public static void main(String[] args) {
        Node node4 = initRootNode();
        findIteratively(node4);
        findRecursively(node4);
    }

    private static Node initRootNode() {
        Node node5 = new Node(null, null, 5);
        Node node7 = new Node(null, node5, 7);
        Node node2 = new Node(null, null, 2);
        Node nodeM1 = new Node(node2, node7, -1);
        Node nodeM4 = new Node(null, null, -4);
        Node node10 = new Node(null, null, 10);
        Node node0 = new Node(node10, nodeM4, 0);
        Node node12 = new Node(node0, null, 12);
        Node node4 = new Node(node12, nodeM1, 4);
//        Node node4 = new Node(null, null, 4);
        return node4;
    }

    private static MinResult init(Node rootNode) {
        return new MinResult(rootNode.getValue(), 1);
    }

    private static void printResult(MinResult minResult) {
        System.out.println("minValue: " + minResult.minValue);
        System.out.println("minValueLevel: " + minResult.minValueLevel);
        System.out.println();
        assert minResult.minValue == -4;
        assert minResult.minValueLevel == 4;
    }

    public static void findIteratively(Node rootNode) {
        System.out.println("findIteratively()");
        MinResult minResult = init(rootNode);
        List<Node> nodes = new ArrayList<>();
        nodes.add(rootNode);
        int level = 1;
        while (!nodes.isEmpty()) {
            int nextLevel = level + 1;
            List<Node> childNodes = new ArrayList<>();
            for (Node node: nodes) {
                minResult = adjustMinResult(minResult, node.getLeft(), nextLevel);
                minResult = adjustMinResult(minResult, node.getRight(), nextLevel);
                addChildNode(childNodes, node.getLeft());
                addChildNode(childNodes, node.getRight());
            }
            nodes.clear();
            nodes.addAll(childNodes);
            level++;
        }
        printResult(minResult);
    }

    private static MinResult adjustMinResult(MinResult minResult, Node node, int level) {
        if (node != null && node.getValue() < minResult.minValue()) {
            return new MinResult(node.getValue(), level);
        } else {
            return minResult;
        }
    }

    private static void addChildNode(List<Node> nodes, Node node) {
        if (node != null) {
            nodes.add(node);
        }
    }
    public static void findRecursively(Node rootNode) {
        System.out.println("findRecursively()");
        MinResult minResult = init(rootNode);
        minResult = find(minResult, rootNode, 1);
        printResult(minResult);
    }

    private static MinResult find(MinResult minResult, Node node, int level) {
        int nextLevel = level + 1;
        minResult = adjustMinResult(minResult, node.getRight(), nextLevel);
        minResult = adjustMinResult(minResult, node.getLeft(), nextLevel);
        if (node.getLeft() != null) {
            minResult = find(minResult, node.getLeft(), nextLevel);
        }
        if (node.getRight() != null) {
            minResult = find(minResult, node.getRight(), nextLevel);
        }
        return minResult;
    }
}
