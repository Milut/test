package ru.mail.milut.test.tsystems.task1;

/**
 * Задача 1: Что будет выведено в консоль и почему? Есть ли в этом коде ошибки? Если есть, то необходимо объяснить почему.
 */
public class C {
    public static void main(String[] args) {
        A a1 = new A();
        A a2 = new B();
//        B a3 = new A();
        B a4 = new B();

        a1.print();
        a1.print1();

        a2.print();
        a2.print1();

//        a3.print();
//        a3.print1();

        a4.print();
        a4.print1();
    }
}
