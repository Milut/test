package ru.mail.milut.test.tsystems.task3.refactored;

public class Parallelepiped extends Rectangle {

    private int height;

    public Parallelepiped(int width, int length, int height) {
        super(width, length);
        this.height = height;
        this.initMeasure();
    }

    @Override
    public void initMeasure() {
        measure = width * length * height;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
