package ru.mail.milut.test.tsystems.task3.refactored;

public class Rectangle {
    protected int width;
    protected int length;
    protected int measure;

    public Rectangle(int width, int length) {
        this.width = width;
        this.length = length;
        this.initMeasure();
    }

    protected void initMeasure() {
        measure = width * length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getMeasure() {
        return measure;
    }
}
