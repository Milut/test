package ru.mail.milut.test.tsystems.task3.refactored;

/**
 * Задача 3: Что будет выведено в консоль и почему? Предложите возможный рефакторинг кода. Есть ли в этом коде ошибки?
 * Если есть, то необходимо объяснить почему. Есть ли в этом коде проблемы(smell)?
 * Если есть, то предложите вариант исправления. Предполагается, что описанные классы составляют часть публичного API,
 * которое может использоваться сторонней системой.
 */
public class OOPTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        test1();
    }

    public static void test1() {
        int width = 100;
        int length = 100;
        int height = 100;
        Parallelepiped room = new Parallelepiped(length, width, height);
        System.out.println("Room volume is " + room.getMeasure());
    }
}
