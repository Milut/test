package ru.mail.milut.test.tsystems.task3;

public class Parallelepiped extends Rectangle {

    private int high;

    public Parallelepiped(int field1, int field2, int field3) {
        super( field1, field2 );
        this.high = field3;
    }

    @Override
    public void mult(){
        measure = width * length * high;
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }
}
