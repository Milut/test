package ru.mail.milut.test.tsystems.task2;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Задача 2: Что будет выведено в консоль и почему? Есть ли в этом коде ошибки? Если есть, то необходимо объяснить почему.
 */
public class C {
    public static void main(String[] args) {
        ArrayList lst = new ArrayList();
        Collection col = lst;
        lst.add("one");
        lst.add("two");
        lst.add("three");
        lst.remove(0);
        col.remove(0);
        System.out.println("Number of elements is: " + lst.size());
        System.out.println("Number of elements is: " + col.size());
    }
}
