package ru.mail.milut.test.tsystems.task4;

/**
 * Задача 4:
 * Напечатать на экране следующее:
 * 1
 * 2-1
 * 1-2-3
 * 4-3-2-1
 * 1-2-3-4-5
 * 6-5-4-3-2-1
 * Вместо 6 может быть любое число,  которое передаётся на вход.
 */
public class CycleTest {
    public static void main(String[] args) {
        int finish = 6;
        testAlternatingCycle(finish);
        testDescendingCycle(finish);
    }

    public static void testAlternatingCycle(int finish) {
        System.out.println("testAlternatingCycle()");
        boolean ascending = true;
        int start = 1;
        for (int i = start; i <= finish; i++) {
            if (ascending) {
                for (int j = start; j <= i; j++) {
                    System.out.print(j);
                    if (j <= i - 1) {
                        System.out.print("-");
                    }
                }
                ascending = false;
            } else {
                for (int j = i; j >= start; j--) {
                    System.out.print(j);
                    if (j > start) {
                        System.out.print("-");
                    }
                }
                ascending = true;
            }
            System.out.println();
        }
    }

    public static void testDescendingCycle(int finish) {
        System.out.println("testDescendingCycle()");
        int start = 1;
        for (int i = start; i <= finish; i++) {
            for (int j = i; j >= start; j--) {
                System.out.print(j);
                if (j > start) {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
}
