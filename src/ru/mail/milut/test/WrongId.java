package ru.mail.milut.test;

class WrongId {
    Integer id;

    public WrongId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "WrongId: id=" + id;
    }
}
