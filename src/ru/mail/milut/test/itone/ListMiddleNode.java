package ru.mail.milut.test.itone;

import ru.mail.milut.test.node.list.Node;

import static ru.mail.milut.test.Utils.initList;
import static ru.mail.milut.test.Utils.printList;

public class ListMiddleNode {
    private static Node findMiddleNode(Node list) {
        Node currentNode = list;
        Node middleNode = currentNode;
        int i = 0;
        while (currentNode.hasNext()) {
            i++;
            if (i == 2) {
                middleNode = middleNode.getNext();
                i = 0;
            }
            currentNode = currentNode.getNext();
        }
        return middleNode;
    }

    private static void testFindMiddleNode(Node list) {
        printList(list);
        System.out.print(" => ");
        System.out.println(findMiddleNode(list));
    }

    private static void testFindMiddleNode() {
        for (int i = 1; i <= 15; i++) {
            testFindMiddleNode(initList(i));
        }
    }

    public static void main(String[] args) {
        testFindMiddleNode();
    }
}
