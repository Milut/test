package ru.mail.milut.test.itone;

public class SuppressedRuntimeExceptionInAutoCloseable {
    private static class TestStream implements AutoCloseable {
        private final String path;

        TestStream(String path) {
            this.path = path;
        }

        void write() {
            throw new RuntimeException("write");
        }

        @Override
        public void close() {
            throw new RuntimeException("close");
        }

    }

    public static void main(String[] args) {
        try (TestStream output = new TestStream("/path")) {
            output.write();
        }
    }
}
