package ru.mail.milut.test.itone;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Найти в строке первую неповторяющуюся букву
 * Например:
 *  "cba" => c
 *  "abc" => a
 *  "abca" => b
 *  "abcab" => c
 *  "abcabcd" => d
 */
class UniqueChar {
    @FunctionalInterface
    private interface FirstUnique {
        // Пишем решение здесь
        Character getFirstUnique(String st);
    }

    public static Character getFirstUnique1(String st) {
        Map<Character, Integer> letters = new LinkedHashMap<>();
        for (int i = 0; i < st.length(); i++) {
            letters.merge(st.charAt(i), 1, Integer::sum);
        }
        return letters.entrySet().stream()
                .filter(letterEntry -> letterEntry.getValue().equals(1))
                .findFirst()
                .map(Map.Entry::getKey).orElse(null);
    }

    public static Character getFirstUnique2(String st) {
        Map<Integer, Integer> letters = new LinkedHashMap<>();
        st.chars().forEach(letter -> letters.merge(letter, 1, Integer::sum));
        return letters.entrySet().stream()
                .filter(letterEntry -> letterEntry.getValue().equals(1))
                .findFirst()
                .map(letter -> (char) letter.getKey().intValue()).orElse(null);
    }

    // Код для проверки
    private static void checkResult(FirstUnique firstUnique) {
        checkResult("cba", firstUnique, 'c');
        checkResult("abc", firstUnique, 'a');
        checkResult("abca", firstUnique, 'b');
        checkResult("abcab", firstUnique, 'c');
        checkResult("abcabcd", firstUnique, 'd');
    }

    private static void checkResult(String input, FirstUnique firstUnique, Character expectedResult) {
        Character actualResult = firstUnique.getFirstUnique(input);
        if (Objects.equals(expectedResult, actualResult)) {
            System.out.println(input + " - OK");
        } else {
            System.out.println("Check failed! Input: '" + input
                    + "'; Result: '" + actualResult + "'; Expected: '" + expectedResult + "'!");
        }
    }

    public static void main(String[] args) {
        System.out.println("getFirstUnique1()");
        checkResult(UniqueChar::getFirstUnique1);
        System.out.println("getFirstUnique2()");
        checkResult(UniqueChar::getFirstUnique2);
    }
}
