package ru.mail.milut.test;

import java.util.Objects;

class RightId {
    Integer id;

    public RightId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RightId rightId = (RightId) o;
        return Objects.equals(id, rightId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "RightId: id=" + id;
    }
}
