package ru.mail.milut.test;

public class MyStuff {
    String name;

    MyStuff(String n) {
        name = n;
    }

    public static void main(String[] args) {
        MyStuff m1 = new MyStuff("guitar");
        MyStuff m2 = new MyStuff("tv");
        System.out.println(m1.equals(m2));
        // prints true
    }

    public boolean equals(Object o) {
        MyStuff m = (MyStuff) o;
        return m.name != null;
    }
}
