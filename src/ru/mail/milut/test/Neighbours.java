package ru.mail.milut.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Дан отсортированный массив целых чисел a, целое число k и индекс элемента index.
 * Необходимо вернуть в любом порядке k чисел из массива, которые являются ближайшими
 * по значению к элементу a[index], не включая сам элемент a[index].
 **/
public class Neighbours {
    public static Integer[] findNeighbours(int[] a, int k, int index) {
        if (
                a == null || a.length <= 1
                        || k <= 0 || k >= a.length
                        || index < 0 || index >= a.length
        ) {
            return new Integer[]{};
        }
        List<Integer> neighboursIndexes = new ArrayList<>(a.length);

        int leftIndex = index - 1;
        int rightIndex = index + 1;
        for (int i = 1; i <= k; i++) {
            if (leftIndex == -1) {
                neighboursIndexes.add(rightIndex);
                rightIndex++;
            } else if (rightIndex == a.length) {
                neighboursIndexes.add(leftIndex);
                leftIndex--;
            } else {
                int leftDiff = a[index] - a[leftIndex];
                int rightDiff = a[rightIndex] - a[index];
                if (leftDiff < rightDiff) {
                    neighboursIndexes.add(leftIndex);
                    leftIndex--;
                } else {
                    neighboursIndexes.add(rightIndex);
                    rightIndex++;
                }
            }
        }
        return neighboursIndexes.toArray(new Integer[0]);
    }

    private static void markElement(int index, String marker) {
        System.out.print("     ");
        for (int i = 1; i <= index; i++) {
            System.out.print("   ");
        }
        System.out.println(marker);
    }

    private static void testNeighbours(int[] a, int index, int k) {
        System.out.println("k = " + k);
        System.out.println("index = " + index);
        System.out.println("a.length = " + a.length);
        System.out.println("a = " + Arrays.toString(a));
        markElement(index, "^");
        Integer[] neighboursIndexes = findNeighbours(a, k, index);
        for (Integer neighboursIndex : neighboursIndexes) {
            markElement(neighboursIndex, "|");
        }
        for (Integer neighboursIndex : neighboursIndexes) {
            System.out.print(a[neighboursIndex] + " ");
        }
    }

    public static void main(String[] args) {
        int[] a = {1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9};
        testNeighbours(a, 0, 5);
        testNeighbours(a, a.length, 5);
        testNeighbours(a, a.length - 1, 5);
        testNeighbours(a, 0, a.length);
        testNeighbours(a, 0, a.length - 1);
        testNeighbours(a, 1, a.length - 1);
        testNeighbours(a, a.length - 1, a.length - 1);
        testNeighbours(a, a.length - 2, a.length - 1);
        testNeighbours(a, 2, a.length - 1);
        testNeighbours(a, a.length - 3, a.length - 1);
        testNeighbours(a, 9, 5);
        // prints 5 6 6 4 4
    }
}
