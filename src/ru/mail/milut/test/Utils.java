package ru.mail.milut.test;

import ru.mail.milut.test.node.list.Node;

import java.util.Random;

public class Utils {
    public static void swap(char[] a, int i, int j) {
        char tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    public static void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    public static int[] getRandomArray() {
        int length = 10;
        int[] array = new int[length];
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }

    public static Node initList(int quantity) {
        Node node = new Node(1, null);
        for (int i = 2; i <= quantity; i++) {
            node.addNode(i);
        }
        return node;
    }

    public static void printList(Node list) {
        Node node = list;
        System.out.print(node.getValue());
        while (node.hasNext()) {
            System.out.print(" ");
            node = node.getNext();
            System.out.print(node.getValue());
        }
    }
}
