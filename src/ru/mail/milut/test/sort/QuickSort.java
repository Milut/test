package ru.mail.milut.test.sort;

import java.util.Arrays;

import static ru.mail.milut.test.Utils.getRandomArray;
import static ru.mail.milut.test.Utils.swap;

/**
 * <a href="https://ru.wikipedia.org/wiki/%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0">Быстрая сортировка</a>
 */
public class QuickSort {
    public static void quickSort(int[] a) {
        quickSort(a, 0, a.length - 1);
    }

    public static void quickSort(int[] a, int lo, int hi) {
        if (lo >= hi) {
            return;
        }
        int p = partition(a, lo, hi);
        quickSort(a, lo, p);
        quickSort(a, p + 1, hi);
    }

    private static int partition(int[] a, int low, int high) {
        int pivot = a[(low + high) / 2];
        int i = low;
        int j = high;
        while (true) {
            while (a[i] < pivot) {
                i++;
            }
            while (a[j] > pivot) {
                j--;
            }
            if (i >= j) {
                return j;
            }
            swap(a, i, j);
            i++;
            j--;
        }
    }

    public static void testQuickSort(int[] a) {
        System.out.println();
        System.out.println("unsorted a = " + Arrays.toString(a));
        quickSort(a);
        System.out.println("sorted   a = " + Arrays.toString(a));
    }

    public static void main(String[] args) {
        testQuickSort(new int[]{});
        testQuickSort(new int[]{1});
        testQuickSort(new int[]{2, 1});
        testQuickSort(new int[]{3, 2, 1});
        testQuickSort(new int[]{7, 6, 5, 4, 3, 2, 1});
        testQuickSort(getRandomArray());
    }
}
