package ru.mail.milut.test.sort;

import java.util.Arrays;

import static ru.mail.milut.test.Utils.getRandomArray;
import static ru.mail.milut.test.Utils.swap;

/**
 * <a href="https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%BF%D1%83%D0%B7%D1%8B%D1%80%D1%8C%D0%BA%D0%BE%D0%BC">Сортировка пузырьком</a>
 */
public class BubbleSort {
    public static void bubbleSort(int[] a) {
        int n = a.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1 - i; j++) {
                if (a[j] > a[j + 1]) {
                    swap(a, j, j + 1);
                }
            }
        }
    }

    private static void testBubbleSort(int[] a) {
        System.out.println();
        System.out.println("unsorted a = " + Arrays.toString(a));
        bubbleSort(a);
        System.out.println("sorted   a = " + Arrays.toString(a));
    }

    public static void main(String[] args) {
        testBubbleSort(new int[]{});
        testBubbleSort(new int[]{1});
        testBubbleSort(new int[]{2, 1});
        testBubbleSort(new int[]{3, 2, 1});
        testBubbleSort(new int[]{7, 6, 5, 4, 3, 2, 1});
        testBubbleSort(getRandomArray());
    }
}
