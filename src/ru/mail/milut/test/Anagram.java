package ru.mail.milut.test;

import java.util.HashMap;
import java.util.Map;

public class Anagram {
    public static boolean isAnagram(String srcString, String testString) {
        if (srcString.length() != testString.length()) {
            return false;
        }
        Map<Integer, Integer> srcChars = chars(srcString);
        Map<Integer, Integer> testChars = chars(testString);
        for (Integer srcKey : srcChars.keySet()) {
            if (!srcChars.get(srcKey).equals(testChars.get(srcKey))) {
                return false;
            }
        }
        return true;
    }

    public static Map<Integer, Integer> chars(String srcString) {
        Map<Integer, Integer> chars = new HashMap<>();
        srcString.chars().forEach(c -> chars.merge(c, 1, Integer::sum));
        return chars;
    }

    public static void main(String[] args) {
        String srcString = "qwerty";
        String testString = "wqeyrt";
        System.out.println(testString + " is anagram of " + srcString + ": " + isAnagram(srcString, testString));
        // prints wqeyrt is anagram of qwerty: true
        srcString = "qweerty";
        testString = "qwertty";
        System.out.println(testString + " is anagram of " + srcString + ": " + isAnagram(srcString, testString));
        // prints qwertty is anagram of qweerty: false
    }
}
