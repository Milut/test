package ru.mail.milut.test;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class IteratorAdapter<T> implements Iterator<T> {
    List<Iterator<T>> iterators;
    int iteratorNumber = 0;

    public IteratorAdapter(List<Iterator<T>> iterators) {
        this.iterators = iterators;
    }

    @Override
    public boolean hasNext() {
        Iterator<T> iterator = getIterator();
        return iterator != null && iterator.hasNext();
    }

    @Override
    public T next() {
        Iterator<T> iterator = getIterator();
        if (iterator != null) {
            iterator.next();
        }
        throw new NoSuchElementException();
    }

    /**
     * May return {@code null}.
     */
    private Iterator<T> getIterator() {
        for (int i = iteratorNumber; i < iterators.size(); i++) {
            Iterator<T> iterator = iterators.get(i);
            if (iterator.hasNext()) {
                iteratorNumber = i;
                return iterator;
            }
        }
        return null;
    }

    public static List<Iterator<String>> test1() {
        return null;
    }

    public static String test2(Iterator<String> stringIterator) {
        return null;
    }

    public static void main(String[] args) {
        List<Iterator<String>> iterators = test1();
        IteratorAdapter<String> iteratorAdapter = new IteratorAdapter<>(iterators);
        String s = test2(iteratorAdapter);
    }
}
