package ru.mail.milut.test;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Что-то мне подсказывает, что типичное. Что представляет собой типичное собеседование, я прекрасно представляю.
 * Вот вам анкетка, поставьте себе оценку по каждому баззворду. Перечислите все методы класса Object, нарисуйте
 * классовую диаграмму для коллекций, исключений, расскажите про MVC паттерн и какие еще названия паттерна знаете,
 * чем SAX парсер от DOM отличается, как работает HashMap, правила переопределения equals и hashCode и как это вообще
 * все работает, какие аннотации в спринге и хибернейте помните, какие существуют стратегии наследования в hibernate,
 * что такое спринг вообще и DI в частности, напишите на бумажке преобразование числа в двоичное представление от
 * начала до конца, с вводом и выводом, ну и тому подобное. Из высокоуровнего — расскажите про предыдущие проекты,
 * сколько там человек, какая степень у вас покрытия кода тестами, какая ваша роль, кем видите себя в нашей фирме
 * через 2 года и т.д. Оно?
 * <p>
 * {@code @Transactional = @Transactional(propagation = Propagation.REQUIRED)}
 */
public class Main {
    public static class IfTest {
        public static void main(String[] args) {
            boolean foo = false;
            if (foo = true) {
                // prints "Never prints" because Assignment 'foo = true' used as condition
                System.out.println("Never prints"); // prints!
            }

            int i;
            boolean b = true;
            if (b) {
                i = 1;
            }
            // compilation error - Variable 'i' might not have been initialized
//            System.out.println(i);
        }
    }

    public static class IdTest {
        public static void main(String[] args) {
            WrongId wrongId1 = new WrongId(1);
            WrongId newWrongId1 = new WrongId(1);
            WrongId wrongId2 = new WrongId(2);
            // prints false
            System.out.println("wrongId1.equals(newWrongId1): " + wrongId1.equals(newWrongId1));
            // prints false
            System.out.println("wrongId1.equals(wrongId2): " + wrongId1.equals(wrongId2));

            Map<WrongId, Integer> wrongMap = new HashMap<>();
            wrongMap.put(wrongId1, wrongId1.id);
            wrongMap.put(newWrongId1, newWrongId1.id);
            wrongMap.put(wrongId2, wrongId2.id);
            // prints {WrongId: id=2=2, WrongId: id=1=1, WrongId: id=1=1}
            System.out.println("wrongMap: " + wrongMap);
            // prints 3
            System.out.println("wrongMap.size(): " + wrongMap.size());

            Set<WrongId> wrongSet = new HashSet<>();
            wrongSet.add(wrongId1);
            wrongSet.add(newWrongId1);
            wrongSet.add(wrongId2);
            // prints [WrongId: id=2, WrongId: id=1, WrongId: id=1]
            System.out.println("wrongSet: " + wrongSet);
            // prints 3
            System.out.println("wrongSet.size(): " + wrongSet.size());

            RightId rightId1 = new RightId(1);
            RightId newRightId1 = new RightId(1);
            RightId rightId2 = new RightId(2);
            // prints true
            System.out.println("rightId1.equals(newRightId1): " + rightId1.equals(newRightId1));
            // prints false
            System.out.println("rightId1.equals(rightId2): " + rightId1.equals(rightId2));

            Map<RightId, Integer> rightMap = new HashMap<>();
            rightMap.put(rightId1, rightId1.id);
            rightMap.put(newRightId1, newRightId1.id);
            rightMap.put(rightId2, rightId2.id);
            // prints {RightId: id=1=1, RightId: id=2=2}
            System.out.println("rightMap: " + rightMap);
            // prints 2
            System.out.println("rightMap.size(): " + rightMap.size());

            Set<RightId> rightSet = new HashSet<>();
            rightSet.add(rightId1);
            rightSet.add(newRightId1);
            rightSet.add(rightId2);
            // prints [RightId: id=1, RightId: id=2]
            System.out.println("rightSet: " + rightSet);
            // prints 2
            System.out.println("rightSet.size(): " + rightSet.size());
        }
    }

    public static class StringTest {
        public static void main(String[] args) {
            String str = "420";
            str += 42;
            // prints 42042
            System.out.print(str);
        }
    }

    public static class ListTest {
        public static void main(String[] args) {
            List<Integer> p = new ArrayList<>();
            p.add(7);
            p.add(3);
            p.add(5);
            p.add(1);
            p.remove(1);
            // prints [7, 5, 1]
            System.out.println(p);
        }
    }

    public static class LoopTest {
        public static void main(String[] args) {
            String[] sa = {"tom ", "jerry "};
            for (int x = 0; x < 3; x++) {
                for (String s : sa) {
                    System.out.print(x + " " + s);
                    if (x == 1) {
                        break;
                    }
                }
            }
            // prints 0 tom 0 jerry 1 tom 2 tom 2 jerry
        }
    }

    public static class ExceptionTest {
        interface I {
            void notThrowException();
            void throwException() throws Exception;
            void throwIOException() throws IOException;
            void notReThrowException() throws Exception;

            private void privateMethod() {
            }
        }

        static class AI implements I {
            @Override
            // notThrowException()' in 'AI' clashes with 'notThrowException()' in 'I';
            // overridden method does not throw 'java.lang.Exception'
//            public void notThrowException() throws Exception {
            public void notThrowException() {
            }

            @Override
            public void throwException() throws IOException {
                throw new IOException();
            }

            @Override
            // 'throwIOException()' in 'AI' clashes with 'throwIOException()' in 'I';
            // overridden method does not throw 'java.lang.Exception'
//            public void throwIOException() throws Exception {
            public void throwIOException() throws IOException {
                throw new IOException();
            }

            @Override
            public void notReThrowException() {
            }

            // Method does not override method from its superclass
//            @Override
            protected void privateMethod() {
            }
        }

        public static void main(String[] args) {
            int x = 10;
            int y = 2;
            try {
                for (int z = 2; z >= 0; z--) {
                    int ans = x / z;
                    System.out.print(ans + " ");
                }
            } catch (ArithmeticException e1) {
                System.out.println("E2");
            } catch (Exception e1) {
                System.out.println("E1");
//            } catch (ArithmeticException e1) {
//                System.out.println("E2");
            }
        }
    }

    public static class GetTest {
        public static void main(String[] args) {
            List<Integer> elements = new ArrayList<>();
            elements.add(10);
            int firstElement = elements.get(1);
            System.out.println(firstElement);
            // IndexOutOfBoundsException
        }
    }

    public static class StringBuilderTest {
        public static void main(String[] args) {
            StringBuilder s1 = new StringBuilder("Java");
            String s2 = "Love";
            s1.append(s2);
            s1.substring(4);
            int foundAt = s1.indexOf(s2);
            System.out.println(foundAt);
            // prints 4
        }
    }

    public static class IndexOfTest {
        public static void main(String[] args) {
//            List<String> items = new ArrayList<>();
//            items.add("Pen");
//            items.add("Pencil");
//            items.add("Box");
            List<String> items = new ArrayList<>() {{
                add("Pen");
                add("Pencil");
                add("Box");
            }};
            for (String i : items) {
                if (i.indexOf("P") == 0) {
                    continue;
                } else {
                    System.out.print(i + " ");
                }
            }
            // prints Box
        }
    }

    public static class NumericOverflow {
        public static void main(String[] args) {
            // Numeric overflow in expression
            final long MICROS_PER_DAY = 24 * 60 * 60 * 1000 * 1000;
            final long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;
            System.out.println(MICROS_PER_DAY / MILLIS_PER_DAY);
            // prints 5
        }
    }

    public static class InconvertibleEquals {
        public static void main(String[] args) {
//            Integer var1 = new Integer(5);
//            Long var2 = new Long(5);
            Integer var1 = 5;
            Long var2 = 5L;
            // 'equals' between objects of inconvertible types 'Integer' and 'Long'
            System.out.println(var1.equals(var2));
            // prints false
        }
    }

    public static class NeverThrownException {
        public static void main(String[] args) {
            try {
                System.out.println("Inside empty try");
            // Exception 'IOException' is never thrown in the corresponding try block
//            } catch (IOException ignore) {
            // includes RuntimeException which is possible!
            } catch (Exception ignore) {
            }
        }
    }

    public static class RegexTest {
        public static void main(String[] args) {
            String source = "ab34ef";
            String regex = "\\d*";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(source);
            while (m.find()) {
//                System.out.print("[" + m.start() + "]" + m.group());
                System.out.print(m.start() + m.group());
            }
            // prints 01234456
        }
    }

    public static class MapTest {
        public static void main(String[] args) {
            Map<String, String> map = new HashMap<>();
            map.put("1", "11");
            map.put("2", "33");
            map.put("3", "33");
            for (String key : map.keySet()) {
                System.out.println("key = " + key + ", value = " + map.get(key));
            }
            for (Map.Entry<String, String> entry : map.entrySet()) {
                System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
            }

            int i1 = 0;
            AtomicInteger i2 = new AtomicInteger();
            map.entrySet().forEach(e -> {
                // Variable used in lambda expression should be final or effectively final
//                i1++;
                i2.incrementAndGet();
                System.out.println("key = " + e.getKey() + ", value = " + e.getValue());
            });
            map.forEach((key, value) -> System.out.println("key = " + key + ", value = " + value));
        }
    }

    public static class GetMaxProfitTest {
        private static double getMaxProfit(double[] prices) {
            double maxProfit = 0.;
            double start = prices[0];
            for (int i = 1; i < prices.length; i++) {
                if (prices[i] > start) {
                    // increasing
                    double delta = prices[i] - start;
                    if (delta > maxProfit) {
                        maxProfit = delta;
                    }
                } else {
                    // decreasing
                    start = prices[i];
                }
            }
            return maxProfit;
        }

        public static void main(String[] args) {
            double[] prices0 = new double[]{5.};
            double[] prices1 = new double[]{5., 2., 4., 3., 5., 1., 2.};
            double[] prices2 = new double[]{5., 2., 4., 3., 5., 1., 2., 6.};
            // 0.
            System.out.println("Max profit for prices0 is " + getMaxProfit(prices0));
            // 3. (5. - 2.)
            System.out.println("Max profit for prices1 is " + getMaxProfit(prices1));
            // 5. (6. - 1.)
            System.out.println("Max profit for prices2 is " + getMaxProfit(prices2));
        }
    }

    public static class PeopleTest {
        public static void main(String[] args) {
//            Set<String> people = Set.of("Глеб", "Сергей", "Женя", "Костя", "Наташа");
            Set<String> people = new LinkedHashSet<>(){{
                add("Глеб");
                add("Сергей");
                add("Женя");
                add("Костя");
                add("Наташа");
            }};
            String first = "";
            int i = 0;
            for (String member : people) {
                i++;
                if (i == 1) {
                    first = member;
                }
                System.out.print(member + "->");
            }
            System.out.println(first);
        }
        // prints Глеб->Сергей->Женя->Костя->Наташа->Глеб
    }

    public static class TreeSetWithComparatorTest {
        public static void main(String[] args) {
            var set = new TreeSet<>((Comparator) (o1, o2) -> 1) {
//            var set = new TreeSet<>() {
                {
                    add(1);
                    add(2);
                    add(3);
                    add(4);
                    add(5);
                    add(6);
                    add(7);
                    add(-10);
                    add(-12);
                    add(13);
                    add(1);
                    add(2);
                    add(3);
                }
            };
            System.out.println(set);
            // prints [1, 2, 3, 4, 5, 6, 7, -10, -12, 13, 1, 2, 3]
        }
    }

    public static class StreamTest {
        public static void main(String[] args) {
            String[] array = {"Java", "Ruuuuussshhh"};

            Stream<String> streamOfArray = Arrays.stream(array);
            streamOfArray.map(s->s.split("")) //Преобразование слова в массив букв
                    .flatMap(Arrays::stream).distinct() //выравнивает каждый сгенерированный поток в один поток
                    .toList().forEach(System.out::println);

            streamOfArray = Arrays.stream(array);
            streamOfArray.map(s->s.split("")) //Преобразование слова в массив букв
                    .map(Arrays::stream).distinct() //Сделать массив в отдельный поток
                    .toList().forEach(System.out::println);

            streamOfArray = Arrays.stream(array);
            streamOfArray.map(s->s.split("")) //Преобразование слова в массив букв
                    .map(Arrays::stream).distinct() //Сделать массив в отдельный поток
                    .toList().forEach(s -> {
                        System.out.println("s = " + s);
                        s.forEach(System.out::println);
                    });
        }
    }
    public static class InterfaceTest {
        interface A {
            void f();
        }

        interface B extends A {
            void f();
        }

        public static void main(String[] args) {
            var c = new B() {
                @Override
                public void f() {
                    System.out.println("f()");
                }
            };
            c.f();
        }
    }

/*
@Transactional = @Transactional(propagation = Propagation.REQUIRED)

https://github.com/piatachki/different_code_test/blob/main/review_3.java

class A {
	String uin;
	String payload;
}

class B {
	String parent;
	String payload;
}

public Collection<Pair<A,B>> join(Collection<A> aCollection, Collection<B> bCollection)
A.uin = B.parent
xhh-tema-pcr
*/

/*
@Service
public class SimpleService {
//    @Transactional
    @Transactional
    public DataObject methodA(DataObject data) {
        methodB(data);
        throw new Exception();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void methodB(DataObject data) {
        repository.save(data);
    }
}

@Component
public class SomeBean {
  	public void someMethod(){
    	List.of(....).stream().paralell()....
			CompletableFuture.async()....map.collect(...)
    }
}
-1,-2,-3,-4,-5
-8

1 2 3 4 5
8

for
	for o(n^2)
-1,-2,-3,-4,-5
-8

1 2 3 4 5
8

1 1 3 4 3
6

for
	for o(n^2)

Map<Integer, List<Integer>>

for  - calc map

for i arr
@Component
@Service
@Controller
@Repository

@Component
public class Transaction {
  	@Autowired
  	private Transaction tr;

    @Transactional
    public void someOp(){
        tr.someOp2();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void someOp2(){
        // do smth
    }
}

tr.someOp();

Order {
  @OneToMany(mappedBy = "order", fetch=FetchType.LAZY)
	private Set<item> items = new HashSet();
}

Item {
  @ManyToOne(joinColumn = "order_id")
  private Order order;
}

@Service
@RequiredArgsConstructor
public class SomeBean {
  private final OrderRepository orderRepository;

  public void someMethod(Long id){
    var order = orderRepository.findById(id);

    order.getItems().stream()......
  }
}

Order {
  @OneToMany(mappedBy = "order", fetch=FetchType.LAZY)
	private Set<item> items = new HashSet();
}

Item {
  @ManyToOne(joinColumn = "order_id")
  private Order order;
}

public interface OrderRepository extends JpaRepository<Order, Long> {
  @EntityGraph(attributePaths = "items")
  Optional<Order> findByIdWithItems(Long id);
}

@Service
@RequiredArgsConstructor
public class SomeBean {
  private final OrderRepository orderRepository;

  public void someMethod(Long id){
    var order = orderRepository.findByIdWithItems(id);

    order.getItems().stream()......
  }
}

LazyInitializationException

@Bean
@Entity
public class Order {
    @Id
    private long id;

    @ManyToOne
    private Customer customer;
}

@Entity
public class Customer {
    @Id
    private long id;

    @OneToMany(mappedBy = "customer") //LAZY
    private List<Order> orders;
}

@Service
public class SimpleService {
    private CrudRepository<Customer> repository;

//    @Transactional
@Transactional
    public void exec() {
        List<Customer> customers = repository.findAll(); // customers.size = 5

        for (Customer customer : customers) {
            for (Order order : customer.getOrders()) { // orders.size = 10
                LOG.info("Order = '" + order + "'");
            }
        }

    }
}
*/

    public static class Test {
        public static void main(String[] args) {
        }
    }

    public static void test() {
        System.out.println("test()");
    }

    public static void main(String[] args) {
        test();
    }
}
