package ru.mail.milut.test.yandex;

/**
 * Инфа про команды и этапы найма:
 * <br>
 * <a href="https://travel.yandex.ru/promo/vac-java">https://travel.yandex.ru/promo/vac-java</a>
 * <br>
 * ___
 * <br>
 * Материалы для подготовки к секциям:
 * <p>
 * 🔝Алгоритмы, которые чаще всего бывают в задачках:
 * <ul>
 *     <li>сортировки (например, bubble sort или quicksort)
 *     <li>разворот одно/двусвязного списка
 *     <li>разворот строки
 *     <li>обход дерева
 * </ul>
 * <p>
 * ➕Примеры наших задач:
 * <p>
 * <a href="https://m.habrahabr.ru/company/yandex/blog/337690/">https://m.habrahabr.ru/company/yandex/blog/337690/</a>
 * <br>
 * <a href="https://m.habrahabr.ru/company/yandex/blog/340784/">https://m.habrahabr.ru/company/yandex/blog/340784/</a>
 * <p>
 * ❓Оценка сложности:
 * <p>
 * <a href="https://habr.com/ru/post/188010/">https://habr.com/ru/post/188010/</a>
 * <p>
 * 📌Подборка по алгоритмам:
 * <p>
 * <a href="https://github.com/tayllan/awesome-algorithms">https://github.com/tayllan/awesome-algorithms</a>
 * <br>
 * <a href="https://m.habr.com/ru/company/yandex/blog/449890/">https://m.habr.com/ru/company/yandex/blog/449890/</a>
 * <br>
 * <a href="https://habr.com/ru/post/78728/">https://habr.com/ru/post/78728/</a>
 * <br>
 * <a href="https://youtu.be/0yxjWwoZtLw">https://youtu.be/0yxjWwoZtLw</a>
 * <br>
 * <a href="https://www.youtube.com/watch?v=zU-LndSG5RE">https://www.youtube.com/watch?v=zU-LndSG5RE</a>
 * <br>
 * Бесплатный курс от Я.Практикум
 * <br>
 * «Подготовка к алгоритмическому собеседованию»
 * <br>
 * <a href="https://practicum.yandex.ru/algorithms-interview/">https://practicum.yandex.ru/algorithms-interview/</a>
 */
public class Main {
}
