package ru.mail.milut.test.inheritance;

interface Rideable {
    String getGait();
}

class Camel implements Rideable {
    int weight = 2;

    // 'getGait()' in 'Camel' clashes with 'getGait()' in 'Rideable';
    // attempting to assign weaker access privileges ('package-private'); was 'public'
//    String getGait() {
    public String getGait() {
        return " mph, lope";
    }

    void go(int speed) {
        ++speed;
        weight++;
        int walkrate = speed * weight;
        System.out.print(walkrate + getGait());
    }
}

public class WeakerAccess {
    public static void main(String[] args) {
        new Camel().go(8);
        // prints 27 mph, lope
    }
}