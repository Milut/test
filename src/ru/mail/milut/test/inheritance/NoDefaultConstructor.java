package ru.mail.milut.test.inheritance;

class Superclass {
    Superclass(int x) {
        System.out.print("Super ");
    }
}

class Subclass extends Superclass {
    // There is no parameterless constructor available in 'Superclass'
//    Subclass() {
//        // Line n1
//        System.out.println("Subclass 2");
//    }
    Subclass(int x) {
        super(x);
        // Line n1
        System.out.println("Subclass " + x);
    }
}

public class NoDefaultConstructor {
    public static void main(String[] args) {
        new Subclass(2);
    }
    // prints Super Subclass 2
}
