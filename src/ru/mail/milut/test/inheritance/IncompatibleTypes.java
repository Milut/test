package ru.mail.milut.test.inheritance;

class Alpha {
    String getType() {
        return "alpha";
    }
}

class Beta extends Alpha {
    String getType() {
        return "beta";
    }
}

class Gamma extends Beta {
    String getType() {
        return "gamma";
    }
}

public class IncompatibleTypes {
    public static void main(String[] args) {
        // compilation error - Incompatible types. Found: 'Alpha', required: 'Gamma'
//        Gamma g1 = new Alpha();
        // compilation error - Incompatible types. Found: 'Beta', required: 'Gamma'
//        Gamma g2 = new Beta();
//        System.out.println(g1.getType() + " " + g2.getType());
        Alpha a1 = new Beta();
        Alpha a2 = new Gamma();
        System.out.println(a1.getType() + " " + a2.getType());
        // prints beta gamma
    }
}
