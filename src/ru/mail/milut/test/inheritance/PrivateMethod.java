package ru.mail.milut.test.inheritance;

class Base {
    private void method() {}
}

class Sub extends Base {
}

public class PrivateMethod {
    public static void main(String[] args) {
        Base base = new Base();
        // compilation error - 'method()' has private access in 'Base'
//        base.method();

        Base baseSub = new Sub();
        // compilation error - 'method()' has private access in 'Base'
//        baseSub.method();

        Sub sub = new Sub();
        // compilation error - 'method()' has private access in 'Base'
//        sub.method();
    }
}
