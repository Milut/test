package ru.mail.milut.test.inheritance;

abstract class Writer {
    public static void write() {
        System.out.println("Writing...");
    }
}

class Author extends Writer {
    public static void write() {
        System.out.println("Writing book");
    }
}

class Programmer extends Writer {
    public static void write() {
        System.out.println("Writing code");
    }
}

public class HidingStaticMethod {
    public static void main(String[] args) {
        Writer w = new Programmer();
        // Static member 'Writer.write()' accessed via instance reference
        w.write();
        // prints Writing...
    }
}
