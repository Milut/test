package ru.mail.milut.test.inheritance;

abstract class Vehicle {
    public int speed() {
        return 0;
    }
}

class Car extends Vehicle {
    public int speed() {
        return 60;
    }
}

class Racecar extends Car {
    public int speed() {
        return 150;
    }
}

public class OverridingMethod {
    public static void main(String[] args) {
        Vehicle vehicle = new Racecar();
        Car car = new Racecar();
        Racecar racer = new Racecar();
        System.out.println("vehicle.speed() = " + vehicle.speed());
        // prints 150
        System.out.println("car.speed() = " + car.speed());
        // prints 150
        System.out.println("racer.speed() = " + racer.speed());
        // prints 150
    }
}
