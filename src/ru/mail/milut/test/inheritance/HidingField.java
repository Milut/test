package ru.mail.milut.test.inheritance;

class Feline {
    public String type = "f ";
    public String toHide = "F ";

    public Feline() {
        System.out.print("feline ");
    }
}

class Cougar extends Feline {
    public String toHide = "C ";
    public Cougar() {
        System.out.print("cougar ");
    }

    void go() {
        type = "c ";
        System.out.print(this.type + super.type + this.toHide + super.toHide);
    }
}

public class HidingField {
    public static void main(String[] args) {
        new Cougar().go();
        // prints feline cougar c c C F
    }
}