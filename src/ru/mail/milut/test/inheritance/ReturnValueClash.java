package ru.mail.milut.test.inheritance;

interface Aa {
    // Modifier 'public' is redundant for interface members
    public void doSomething(String thing);
}

class AImpl implements Aa {
    public void doSomething(String msg) {
    }
}

class Bb {
    public Aa doIt() {
        // more code here
        return null;
    }

    public String execute() {
        // more code here
        return null;
    }
}

class Cc extends Bb {
    public AImpl doIt() {
        // more code here
        return null;
    }

    // 'execute()' in 'Cc' clashes with 'execute()' in 'Bb';
    // attempting to use incompatible return type
//    public Object execute() {
    public String execute() {
        // more code here
        return null;
    }
}

public class ReturnValueClash {
    public static void main(String[] args) {
        new Cc();
    }
}
