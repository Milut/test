package ru.mail.milut.test.inheritance;

class Building {
}

class Barn extends Building {

}

public class Casting {
    public static void main(String[] args) {
        Building building1 = new Building();
        Barn barn1 = new Barn();
        // Casting 'building1' to 'Barn' will produce 'ClassCastException' for any non-null value
        Barn barn2 = (Barn) building1;
        // Casting 'building1' to 'Object' is redundant
        Object obj1 = (Object) building1;
        // Inconvertible types; cannot cast 'Building' to 'java.lang.String'
//        String str1 = (String) building1;
        // Casting 'barn1' to 'Building' is redundant
        Building building2 = (Building) barn1;
    }
}
