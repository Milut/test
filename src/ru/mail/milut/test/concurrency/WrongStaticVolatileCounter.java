package ru.mail.milut.test.concurrency;

/**
 * Non-atomic operation on volatile field 'counter'
 */
public class WrongStaticVolatileCounter {
    private static volatile Integer counter = 0;
//    private static Integer counter = 0;

    public static void main(String[] args) {
        int numIncrement1 = 1000;
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < numIncrement1; i++) {
                increment();
            }
        });

        int numIncrement2 = 2000;
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < numIncrement2; i++) {
                increment();
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Counter: " + counter);
        int sumNumIncrement = numIncrement1 + numIncrement2;
        if (counter != sumNumIncrement) {
            throw new RuntimeException("Wrong " + counter + " counter (must be " + sumNumIncrement +  ")!");
        }
    }

//    private static synchronized void increment() {
    private static void increment() {
        // Non-atomic operation on volatile field 'counter'
        counter++;
    }
}
