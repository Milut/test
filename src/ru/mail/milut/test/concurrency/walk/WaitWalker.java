package ru.mail.milut.test.concurrency.walk;

import static ru.mail.milut.test.concurrency.walk.Side.LEFT;
import static ru.mail.milut.test.concurrency.walk.Side.RIGHT;

class Runner implements Runnable {
    private static final int STEP_QUANTITY = 1000;
    private final WaitWalker waitWalker;
    private final Side side;

    public Runner(WaitWalker waitWalker, Side side) {
        this.waitWalker = waitWalker;
        this.side = side;
    }

    @Override
    public void run() {
        for (int i = 1; i <= STEP_QUANTITY; i++) {
            waitWalker.step(side);
        }
    }
}

public class WaitWalker {
    private int diff = 0;
    private final StringBuilder steps = new StringBuilder();
    private boolean leftStep = true;

    public int getDiff() {
        return diff;
    }

    public StringBuilder getSteps() {
        return steps;
    }

    public synchronized void step(Side side) {
        if (side == LEFT) {
            stepLeft();
        } else if (side == RIGHT) {
            stepRight();
        }
    }

    private void stepLeft() {
        while (!leftStep) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        steps.append(LEFT);
        diff++;
        leftStep = false;
        notifyAll();
    }

    private void stepRight() {
        while (leftStep) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        steps.append(RIGHT);
        diff--;
        leftStep = true;
        notifyAll();
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Calculating...");
        WaitWalker waitWalker = new WaitWalker();
        Thread lefter = new Thread(new Runner(waitWalker, LEFT), "Lefter");
        Thread righter = new Thread(new Runner(waitWalker, RIGHT), "Righter");
        lefter.start();
        righter.start();
        lefter.join();
        righter.join();
        System.out.println();
        System.out.println("diff = " + waitWalker.getDiff());
//        System.out.println("steps = " + waitWalker.getSteps());
        if (waitWalker.getSteps().indexOf(LEFT.name() + LEFT.name()) != -1
                || waitWalker.getSteps().indexOf(RIGHT.name() + RIGHT.name()) != -1) {
            System.out.println("Error!");
        }
    }
}
