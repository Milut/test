package ru.mail.milut.test.concurrency.walk;

import static ru.mail.milut.test.concurrency.walk.Side.LEFT;
import static ru.mail.milut.test.concurrency.walk.Side.RIGHT;

public class UnsynchronizedWalker {
    private static final int STEP_QUANTITY = 1000;
    private int diff = 0;
    private final StringBuilder steps = new StringBuilder();
    private boolean leftStep = true;

    public void walk() throws InterruptedException {
        System.out.println("Calculating...");
        Thread lefter = new Thread(() -> {
            try {
                step(LEFT);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "Lefter");
        Thread righter = new Thread(() -> {
            try {
                step(RIGHT);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }, "Righter");
        lefter.start();
        righter.start();
        lefter.join();
        righter.join();
        System.out.println();
//        System.out.println("steps = " + steps);
        System.out.println("diff = " + diff);
        if (steps.indexOf(LEFT.name() + LEFT.name()) != -1
                || steps.indexOf(RIGHT.name() + RIGHT.name()) != -1) {
            System.out.println("Error!");
        }
    }

    private void step(Side side) throws InterruptedException {
        int i = 1;
        do {
            Thread.sleep(1);
            if (side == LEFT && leftStep) {
//                System.out.print("<");
//                System.out.print(side);
                steps.append(side);
                diff++;
            }
            if (side == RIGHT && !leftStep) {
//                System.out.print(">");
//                System.out.print(side);
                steps.append(side);
                diff--;
            }
            if (side == LEFT && leftStep || side == RIGHT && !leftStep) {
                leftStep = !leftStep;
            }
            i++;
        } while (i <= STEP_QUANTITY);
    }

    public static void main(String[] args) throws InterruptedException {
        new UnsynchronizedWalker().walk();
    }
}
