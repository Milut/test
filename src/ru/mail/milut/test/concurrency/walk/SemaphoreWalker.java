package ru.mail.milut.test.concurrency.walk;

import java.util.concurrent.Semaphore;

import static ru.mail.milut.test.concurrency.walk.Side.LEFT;
import static ru.mail.milut.test.concurrency.walk.Side.RIGHT;

public class SemaphoreWalker {
    private int diff = 0;
    private final StringBuilder steps = new StringBuilder();
    private boolean leftStep = true;
    private final Semaphore semaphore = new Semaphore(1);

    public int getDiff() {
        return diff;
    }

    public StringBuilder getSteps() {
        return steps;
    }

    private static class Runner implements Runnable {
        private static final int STEP_QUANTITY = 1000;
        private final SemaphoreWalker semaphoreWalker;
        private final Side side;

        public Runner(SemaphoreWalker semaphoreWalker, Side side) {
            this.semaphoreWalker = semaphoreWalker;
            this.side = side;
        }

        @Override
        public void run() {
            for (int i = 1; i <= STEP_QUANTITY; i++) {
                semaphoreWalker.step(side);
            }
        }
    }

    public void step(Side side) {
        if (side == LEFT) {
            stepLeft();
        } else if (side == RIGHT) {
            stepRight();
        }
    }

    private void stepLeft() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (leftStep) {
            steps.append(LEFT);
            diff++;
            leftStep = false;
        }
        semaphore.release();
    }

    private void stepRight() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (!leftStep) {
            steps.append(RIGHT);
            diff--;
            leftStep = true;
        }
        semaphore.release();
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Calculating...");
        SemaphoreWalker semaphoreWalker = new SemaphoreWalker();
        Thread lefter = new Thread(new Runner(semaphoreWalker, LEFT), "Lefter");
        Thread righter = new Thread(new Runner(semaphoreWalker, RIGHT), "Righter");
        lefter.start();
        righter.start();
        lefter.join();
        righter.join();
        System.out.println();
        System.out.println("diff = " + semaphoreWalker.getDiff());
//        System.out.println("steps = " + semaphoreWalker.getSteps());
        if (semaphoreWalker.getSteps().indexOf(LEFT.name() + LEFT.name()) != -1
                || semaphoreWalker.getSteps().indexOf(RIGHT.name() + RIGHT.name()) != -1) {
            System.out.println("Error!");
        }
    }
}
