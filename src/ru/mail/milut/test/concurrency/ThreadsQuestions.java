package ru.mail.milut.test.concurrency;

public class ThreadsQuestions {
    public void go() {
        Runnable r = () -> System.out.print("foo");
        Thread t = new Thread(r);
        t.start();
        // IllegalThreadStateException
        t.start();
    }

    public static void main(String[] args) {
        new ThreadsQuestions().go();
    }
}
