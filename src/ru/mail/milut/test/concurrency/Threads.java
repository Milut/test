package ru.mail.milut.test.concurrency;

import java.util.concurrent.atomic.AtomicInteger;

public class Threads {
    AtomicInteger name = new AtomicInteger();
    int x = 0;

    public void incrementX(int number, int current) {
        current = x;
        System.out.println("[" + number + "] " + current);
        x = current + 2;
    }

    public class Runner implements Runnable {
        public void run() {
            doRun();
        }
    }

    public void doRun() {
//    public synchronized void doRun() {
        int number = name.incrementAndGet();
        int current = 0;
        for (int i = 0; i < 4; i++) {
//            incrementX(number, current);
            synchronized (this) {
                incrementX(number, current);
            }
        }
        System.out.println("[" + number + "] x = " + x);
    }

    public void go() throws InterruptedException {
        Runnable r1 = new Runner();
        Thread t1 = new Thread(r1, "Thread1");
        t1.start();
        Thread t2 = new Thread(r1, "Thread2");
        t2.start();
        t1.join();
        t2.join();
    }

    public static void main(String[] args) throws InterruptedException {
        Threads threads = new Threads();
        threads.go();
        System.out.println("x = " + threads.x);
        // prints x = 16
    }
}
