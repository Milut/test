package ru.mail.milut.test.concurrency;

import java.util.concurrent.TimeUnit;

/**
 * volatile needed to stop!
 */
public class VolatileNeededToStopThread {
//    private static boolean stopRequested;
        private static volatile boolean stopRequested;

    public static void main(String[] args) throws InterruptedException {
//            AtomicInteger i = new AtomicInteger();
        Thread backgroundThread = new Thread(() -> {
            int i = 0;
            while (!stopRequested) {
                i++;
//                    i.getAndIncrement();
//                    System.out.println("i = " + i);
            }
        });
        backgroundThread.setName("backgroundThread");
        backgroundThread.start();
        System.out.println("waiting...");
        TimeUnit.SECONDS.sleep(1);
        stopRequested = true;
//            System.out.println("i = " + i);
//            System.out.println();
//        System.exit(0);
    }
}
