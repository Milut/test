package ru.mail.milut.test.concurrency;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Increment {
    private static int counter1 = 0;
    private static int counter2 = 0;

//    private static void incrementCounters() {
    private static synchronized void incrementCounters() {
        counter1++;
        counter2++;
    }

    public static void main(String[] args) throws InterruptedException {
        int tasksCount = 100000;
        CountDownLatch latch = new CountDownLatch(tasksCount);
        ExecutorService executor = Executors.newFixedThreadPool(100);
        for (int i = 0; i < tasksCount; i++) {
            executor.submit(() -> {
                incrementCounters();
                latch.countDown();
                try {
                    TimeUnit.MILLISECONDS.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
        }

//        System.out.println(counter1);
//        System.out.println(counter2);
        synchronized (Increment.class) {
            System.out.println(counter1);
            System.out.println(counter2);
        }
        System.out.println("awaiting...");

        latch.await();

        System.out.print(counter1 + " ");
        System.out.println(counter2);
        // prints 100000 100000
        System.exit(0);
    }
}
