/*
Таблица parent
id : integer (pk)
name : varchar

Таблица child
id : integer (pk)
name : varchar

Таблица parent_child
parent_id : integer (fk)
child_id : integer (fk)
pk (parent_id, child_id)
*/
CREATE TABLE parent(
   id SERIAL PRIMARY KEY NOT NULL,
   name VARCHAR(255) NOT NULL
);

INSERT INTO parent (id, name) VALUES
(1, 'Иван'),
(2, 'Пётр'),
(3, 'Василий'),
(4, 'Иван'),
(5, 'Пётр'),
(6, 'Василий');

CREATE TABLE child(
   id SERIAL PRIMARY KEY NOT NULL,
   name VARCHAR(255) NOT NULL
);

INSERT INTO child (id, name) VALUES
(1, 'Ваня'),
(2, 'Петя'),
(3, 'Вася'),
(4, 'Ваня'),
(5, 'Петя'),
(6, 'Вася');

CREATE TABLE parent_child(
   parent_id INT NOT NULL,
   child_id INT NOT NULL,
   FOREIGN KEY (parent_id) REFERENCES parent (id),
   FOREIGN KEY (child_id) REFERENCES child (id),
   PRIMARY KEY (parent_id, child_id)
);

INSERT INTO parent_child (parent_id, child_id) VALUES
(1, 5),
(2, 6),
(3, 1),
(4, 3),
(5, 4),
(6, 2);

-- Найти всех родителей, у кого есть ребенок с именем "Иван"
select parent.id, parent.name from parent, parent_child, child
where parent.id = parent_child.parent_id
and parent_child.child_id = child.id
and child.name = 'Ваня';

 -- GROUP BY - HAVING - SUM() --

-- Persons со списком работников
CREATE TABLE Persons(
   Id SERIAL PRIMARY KEY NOT NULL,
   Name VARCHAR(255) NOT NULL
);

INSERT INTO Persons (Id, Name) VALUES
(1, 'Petya'),
(2, 'Vasya'),
(3, 'Kolya');

-- Payments с зарплатными начислениями ежемесячно.
CREATE TABLE Payments(
   ID SERIAL PRIMARY KEY NOT NULL,
   Persons_id INT,
   Value NUMERIC,
   FOREIGN KEY (Persons_id) REFERENCES Persons (Id)
);

INSERT INTO Payments (Id, Persons_id, Value) VALUES
(1, 1, 10),
(2, 1, 20),
(3, 3, 15);
-- Связь между таблицами один ко многим, причем у сотрудника может не быть несколько выплат
-- либо не быть ни одной выплаты, если он только пришел в компанию

-- необходимо написать запрос, чтобы выбрать всех сотрудников вместе с их зарплатными начислениями.
select Name, Value
from Persons left join Payments on Persons.Id = Payments.Persons_id;

select Persons.Name
from Persons left join Payments on Persons.Id = Payments.Persons_id
group by Payments.Persons_id;

select Persons.Name, sum(Payments.Value) as sum_payments
from Persons left join Payments on Persons.Id = Payments.Persons_id
group by Payments.Persons_id;

select sum(Payments.Value) as sum_payments
from Payments;

select sum(Payments.Value) as sum_payments
from Payments
group by Payments.Persons_id;

select Name, count(Payments.Persons_id) as num_payments
from Persons left join Payments on Persons.Id = Payments.Persons_id
group by Payments.Persons_id;

-- выбрать сотрудников у которых общий размер начислений превышает 15
select Persons.Id, Persons.Name, sum(Payments.Value) as sum_payments
from Persons left join Payments on Persons.Id = Payments.Persons_id
group by Persons.Id
having sum_payments > 15;

select Persons.Name, sum(Payments.Value) as sum_payments
from Persons left join Payments on Persons.Id = Payments.Persons_id
group by Payments.Persons_id
having sum_payments > 15;

select Persons.Name /* там, где один */, sum(Payments.Value) /* там, где много */ as sum_payments
-- связь один-ко-многим
from Persons /* там, где один */ left join Payments /* там, где много */
on Persons.Id /* там, где один */ = Payments.Persons_id /* там, где много */
group by Payments.Persons_id /* там, где много */
having sum_payments > 15; /* там, где много */

-- GROUP BY - HAVING - AVG() --

CREATE TABLE DEPARTMENTS(
   DEPARTMENT_ID SERIAL PRIMARY KEY NOT NULL,
   DEPARTMENT_NAME VARCHAR(255) NOT NULL
);

CREATE TABLE EMPLOYEES(
   EMP_ID SERIAL PRIMARY KEY NOT NULL,
   SURNAME VARCHAR(255) NOT NULL,
   SALARY NUMERIC,
   DEPARTMENT_ID INT,
   FOREIGN KEY (DEPARTMENT_ID) REFERENCES DEPARTMENTS (DEPARTMENT_ID)
);

INSERT INTO DEPARTMENTS (DEPARTMENT_ID, DEPARTMENT_NAME) VALUES
(1, 'HR'),
(2, 'IT'),
(3, 'FIN');

INSERT INTO EMPLOYEES (EMP_ID, SURNAME, SALARY, DEPARTMENT_ID) VALUES
(1, 'Ivanov', 7000.0, 1),
(2, 'Petrov', 9000.0, 1),
(3, 'Sidorov', 12000.0, 2),
(4, 'Lineytseva', 4000.0, 3),
(5, 'Godunova', 5000.0, 3);

/*
Даны две таблицы:
EMPLOYEES
---------------|---------
EMP_ID         | NUMBER PK
SURNAME        | VARCHAR
SALARY         | NUMBER
DEPARTMENT_ID  | NUMBER FK

DEPARTMENTS
----------------|---------
DEPARTMENT_ID   | NUMBER PK
DEPARTMENT_NAME | VARCHAR

Получить список департаментов и среднюю з/п по департаменту, где средняя з/п по департаменту больше 5000.

Ожидаемый результат:
DEPARTMENT_NAME         AVG_SALARY
HR                          8000
IT                         12000
*/
SELECT DEPARTMENTS.DEPARTMENT_NAME, COUNT(EMPLOYEES.DEPARTMENT_ID) AS NUM_EMPLOYEES
FROM DEPARTMENTS INNER JOIN EMPLOYEES ON DEPARTMENTS.DEPARTMENT_ID = EMPLOYEES.DEPARTMENT_ID
GROUP BY EMPLOYEES.DEPARTMENT_ID
HAVING NUM_EMPLOYEES > 1;

SELECT DEPARTMENTS.DEPARTMENT_NAME, AVG(EMPLOYEES.SALARY) AS AVG_SALARY
FROM EMPLOYEES, DEPARTMENTS
WHERE EMPLOYEES.DEPARTMENT_ID = DEPARTMENTS.DEPARTMENT_ID
GROUP BY DEPARTMENTS.DEPARTMENT_NAME
HAVING AVG_SALARY > 5000;

SELECT DEPARTMENTS.DEPARTMENT_NAME, AVG(EMPLOYEES.SALARY) AS AVG_SALARY
FROM EMPLOYEES INNER JOIN DEPARTMENTS ON EMPLOYEES.DEPARTMENT_ID = DEPARTMENTS.DEPARTMENT_ID
GROUP BY DEPARTMENTS.DEPARTMENT_NAME
HAVING AVG_SALARY > 5000;

SELECT DEPARTMENTS.DEPARTMENT_NAME, AVG(EMPLOYEES.SALARY) AS AVG_SALARY
FROM DEPARTMENTS INNER JOIN EMPLOYEES ON DEPARTMENTS.DEPARTMENT_ID = EMPLOYEES.DEPARTMENT_ID
GROUP BY EMPLOYEES.DEPARTMENT_ID
HAVING AVG_SALARY > 5000;

SELECT DEPARTMENTS.DEPARTMENT_NAME /* там, где один */, AVG(EMPLOYEES.SALARY) /* там, где много */ AS AVG_SALARY
-- связь один-ко-многим
FROM DEPARTMENTS /* там, где один */ INNER JOIN EMPLOYEES /* там, где много */
ON DEPARTMENTS.DEPARTMENT_ID /* там, где один */ = EMPLOYEES.DEPARTMENT_ID /* там, где много */
GROUP BY EMPLOYEES.DEPARTMENT_ID /* там, где много */
HAVING AVG_SALARY > 5000; /* там, где много */
